{ pkgs, ... }:

{
    dconf.settings = {
        "org/gnome/desktop/calendar" = {
            show-weekdate = true;
        };

        "org/gnome/desktop/interface" = {
            color-scheme = "prefer-dark";
            cursor-theme = "capitaine-cursors-white";
            gtk-theme = "adw-gtk3-dark";
            icon-theme = "Papirus-Dark";
        };

        "org/gnome/desktop/wm/keybindings" = {
            switch-applications = [];
            switch-applications-backward = [];
            switch-windows = [ "<Alt>Tab" ];
            switch-windows-backward = [ "<Shift><Alt>Tab" ];
        };

        "org/gnome/desktop/wm/preferences" = {
            button-layout = "appmenu:minimize,close";
        };

        "org/gnome/mutter" = {
            attach-modal-dialogs = true;
            center-new-windows = true;
            dynamic-workspaces = true;
            edge-tiling = true;
            workspaces-only-on-primary = true;
        };

        "org/gnome/shell" = {
            disable-user-extensions = false;
            enabled-extensions = [
                "appindicatorsupport@rgcjonas.gmail.com"
                "blur-my-shell@aunetx"
                "CoverflowAltTab@palatis.blogspot.com"
                "rounded-window-corners@yilozt"
                "user-theme@gnome-shell-extensions.gcampax.github.com"
            ];
        };
    
        "org/gnome/shell/app-switcher" = {
            current-workspace-only = true;
        };
    };

    home = {
        pointerCursor = {
            name = "Papirus-Dark";
            package = pkgs.capitaine-cursors;
            size = 24;
            gtk.enable = true;
        };

        packages = with pkgs; [
            adw-gtk3
            papirus-icon-theme

            gnomeExtensions.appindicator
            gnomeExtensions.blur-my-shell
            gnomeExtensions.coverflow-alt-tab
            gnomeExtensions.rounded-window-corners
            gnomeExtensions.user-themes
        ];
    };
}
