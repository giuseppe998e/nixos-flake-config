{ config, pkgs, lib, ... }:

with lib;

{
    services = {
        xserver = {
            enable = true;

            # GDM display manager
            displayManager.gdm = {
                enable = true;
                wayland = true;
            };

            # Gnome desktop environment
            desktopManager.gnome.enable = true;

            # Disable xTerm auto-install
            excludePackages = with pkgs; [
                xterm
            ];
        };

        gnome = {
            core-developer-tools.enable = mkForce false;
            core-utilities.enable = mkForce false;
            evolution-data-server.enable = mkForce false;
            games.enable = mkForce false;
            gnome-browser-connector.enable = mkForce true;
            gnome-online-miners.enable = mkForce false;
            tracker-miners.enable = mkForce false;
            tracker.enable = mkForce false;
        };

        udev.packages = with pkgs; [
            gnome.gnome-settings-daemon
        ];
    };

    environment = {
        systemPackages = with pkgs; [
            evince
            gnome-text-editor

            gnome.eog
            gnome.file-roller
            gnome.gnome-disk-utility
            gnome.gnome-system-monitor
            gnome.nautilus
        ];

        gnome.excludePackages = with pkgs; [
            gnome-online-accounts
            gnome-tour
            gnome.gnome-shell-extensions
        ];

        sessionVariables = {
            EDITOR = "gnome-text-editor";
            NIXOS_OZONE_WL = "1";
        };
    };

    xdg = {
        portal = {
            enable = true;
            xdgOpenUsePortal = true;
            extraPortals = with pkgs; [
                xdg-desktop-portal-gnome
            ];
        };
    };
}
