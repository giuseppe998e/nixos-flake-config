{ pkgs, lib, ... }:

{
    services.pipewire = {
        enable = true;

        alsa.enable = true;
        #jack.enable = true;
        pulse.enable = true;
        wireplumber.enable = true;
    };

    # Required
    hardware.pulseaudio.enable = lib.mkForce false;

    # Recommended
    security.rtkit.enable = true;
}
