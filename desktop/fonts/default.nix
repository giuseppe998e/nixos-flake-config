{ pkgs, ... }:

let
    nerd-fonts-multi = pkgs.nerdfonts.override {
        fonts = [ "FiraCode" "Hack" "JetBrainsMono" ];
    };
in {
    fonts.fonts = with pkgs; [
        cantarell-fonts
        corefonts
        dejavu_fonts
        liberation_ttf
        nerd-fonts-multi
        noto-fonts
        noto-fonts-cjk
        noto-fonts-emoji
    ];
}
