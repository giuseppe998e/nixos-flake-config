{ pkgs, ... }:

{
    boot = {
        # Kernel version
        kernelPackages = pkgs.linuxPackages_latest;

        # Runtime parameters of the Linux kernel, as set by sysctl
        kernel.sysctl = {
            "vm.swappiness" = 10;
            "vm.vfs_cache_pressure" = 50;
        };

        # Initial ramdisk
        initrd = {
            # Names of supported filesystem types in the initial ramdisk
            supportedFilesystems = [ "ext4" "btrfs" "tmpfs" ];
        };

        # Boot loader
        loader = {
            efi.canTouchEfiVariables = true;
            systemd-boot = {
                enable = true;
                editor = false;
                configurationLimit = 3;
            };
        };
    };
}
