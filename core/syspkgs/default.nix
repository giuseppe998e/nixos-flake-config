{ pkgs, ... }:

{
    imports = [
        ./zsh.nix
    ];

    environment.systemPackages = with pkgs; [
        btop
        git
        neovim
        unzip
        wget
        zip
    ];
}
