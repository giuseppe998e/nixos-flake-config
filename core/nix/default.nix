{ pkgs, ... }:

{
    nix = {
        package = pkgs.nixVersions.stable;

        gc = {
            automatic = true;
            dates = "weekly";
            options = "--delete-older-than-7d";
        };

        settings = {
            auto-optimise-store = true;
            builders-use-substitutes = true;
            experimental-features = [ "nix-command" "flakes" ];
            keep-derivations = true;
            keep-outputs = true;
            trusted-users = [ "root" "@wheel" ];
        };
    };

    nixpkgs.config.allowUnfree = true;
}
