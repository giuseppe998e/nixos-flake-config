{ osConfig, pkgs, lib, ... }:

let
    fontFamily = "Hack Nerd Font Mono";
    gnomeEnabled = osConfig.services.xserver.desktopManager.gnome.enable;
in {
    programs.alacritty = {
        enable = true;
        settings = {
            window = {
                dinamic_title = true;
                opacity = 0.9;
                dimensions = {
                    columns = 160;
                    lines = 32;
                };
            };

            scrolling = {
                history = 10000;
                multiplier = 3;
            };

            font = {
                normal = {
                    family = fontFamily;
                    style = "Regular";
                };
                bold = {
                    family = fontFamily;
                    style = "Bold";
                };
                italic = {
                    family = fontFamily;
                    style = "Italic";
                };
                bold_italic = {
                    family = fontFamily;
                    style = "Bold Italic";
                };
            };

            mouse.hide_when_typing = true;
            mouse_bindings = [
                {
                    mouse = "Middle";
                    action = "PasteSelection";
                }
            ];
        };
    };

    dconf.settings = lib.mkIf gnomeEnabled {
        "org/gnome/settings-daemon/plugins/media-keys" = {
            custom-keybindings = [
                "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/alacritty/"
            ];
        };

        "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/alacritty" = {
            binding = "<Control><Alt>t";
            command = "alacritty";
            name = "Alacritty";
        };
    };

    home.sessionVariables = {
        TERM = "alacritty";
    };
}
