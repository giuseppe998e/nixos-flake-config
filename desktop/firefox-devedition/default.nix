{ osConfig, pkgs, lib, ... }:

let
    services = osConfig.services;
    gnomeEnabled = services.xserver.desktopManager.gnome.enable;
    gnomeBrowserConnectorEnabled = services.gnome.gnome-browser-connector.enable;
in {
    programs.firefox = {
        enable = true;

        package = pkgs.firefox-devedition.override {
            cfg.enableGnomeExtensions = (gnomeEnabled && gnomeBrowserConnectorEnabled);
        };

        profiles.dev-edition-default = {
            isDefault = true;

            extensions = with pkgs.nur.repos.rycee.firefox-addons; [
                facebook-container
                privacy-possum
                search-by-image
                sponsorblock
                ublock-origin
                violentmonkey
            ] ++ lib.optionals (gnomeEnabled && gnomeBrowserConnectorEnabled) [
                gnome-shell-integration
            ];

            #settings = {
            #    # Don't allow websites to prevent copy and paste. Disable
            #    # notifications of copy, paste, or cut functions. Stop webpage
            #    # knowing which part of the page had been selected.
            #    "dom.event.clipboardevents.enabled" = true;
            #
            #    # Do not track from battery status.
            #    "dom.battery.enabled" = false;
            #
            #    # Disable site reading installed plugins.
            #    "plugins.enumerable_names" = "";
            #
            #    # Firefox experiments...
            #    "experiments.activeExperiment" = false;
            #    "experiments.enabled" = false;
            #    "experiments.supported" = false;
            #    "extensions.pocket.enabled" = false;
            #    "identity.fxaccounts.enabled" = false;
            #
            #    # Telemetry
            #    "toolkit.telemetry.enabled" = false;
            #    "toolkit.telemetry.unified" = false;
            #    "toolkit.telemetry.archive.enabled" = false;
            #
            #    # TBD...
            #};
        };
    };
}
