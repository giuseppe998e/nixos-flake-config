{
    description = "My system configuration";

    inputs = {
        # NixOS rolling release
        nixpkgs.url = "nixpkgs/nixos-unstable";

        # Nix User Repository (NUR)
        nur.url = "github:nix-community/NUR";

        # Nix-based user environment manager
        home-manager = {
            url = "github:nix-community/home-manager";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        # Overlay for (binary distributed) rust toolchains
        rust-overlay = {
            url = "github:oxalica/rust-overlay";
            inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { self
        , nixpkgs
        , nur
        , home-manager
        , rust-overlay
        , ...
    } @ inputs:
    let
        #inherit (nixpkgs.lib) attrNames filterAttrs hasSuffix readDir;
        #lib = nixpkgs.lib.extend (self: super: {
        #    listConfig = dir:
        #        map (name: dir + "/${name}")
        #            (attrNames (filterAttrs
        #                (name: _: hasSuffix ".nix" name)
        #                (readDir dir)
        #            ));
        #});

        pkgs = import nixpkgs {
            system = defaultSystem; # variable below
            config.allowUnfree = true;
        };

        common = { pkgs, config, ... }: {
            config = {
                nix.settings = {
                    trusted-public-keys = [
                        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
                        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
                    ];

                    substituters = [ "https://cache.nixos.org" "https://nix-community.cachix.org" ];
                };

                nixpkgs.overlays = [
                    nur.overlay
                    rust-overlay.overlays.default
                ];
            };
        };

        homemgr = [
            home-manager.nixosModules.home-manager {
                home-manager = {
                    useGlobalPkgs = true;
                    useUserPackages = true;
                    #users.*USERNAME* = nixpkgs.lib.mkMerge [ ./dotfiles/*USERNAME* ];
                };
            }
        ];

        # Output defaults
        defaultSystem = "x86_64-linux";
        defaultModules = [ common ] ++ homemgr;
    in {
        nixosConfigurations = {
            # Virtual Machine
            virtm = nixpkgs.lib.nixosSystem {
                system = defaultSystem;
                modules = [ ./host/virtm ] ++ defaultModules;
                specialArgs = {
                    inherit inputs;
                    inherit home-manager;
                };
            };
        };
    };
} # Inspired by "https://github.com/pimeys/nixos"

