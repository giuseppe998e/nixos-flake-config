{ inputs, lib, pkgs, ... }:

{
    imports = [
        ./hardware.nix
        ../../core
        ../../desktop
    ];

    time.timeZone = "Europe/Rome";
    i18n.defaultLocale = "en_US.UTF-8";
    console.keyMap = "it";

    users.users.giuseppe.initialPassword = "nixos";
}
