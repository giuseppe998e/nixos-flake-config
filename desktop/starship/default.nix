{
    programs.starship = {
        enable = true;

        settings = {
            directory.style = "bold blue";
            battery.disabled = true;
        };
    };
}
