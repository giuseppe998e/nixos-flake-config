{ pkgs, ... }:

{
    users.defaultUserShell = pkgs.zsh;

    programs.zsh = {
        enable = true;

        autosuggestions.enable = true;
        syntaxHighlighting.enable = true;

        setOptions = [
            "HIST_EXPIRE_DUPS_FIRST"
            "HIST_IGNORE_DUPS"
            "HIST_IGNORE_ALL_DUPS"
            "HIST_FIND_NO_DUPS"
            "HIST_IGNORE_SPACE"
            "HIST_SAVE_NO_DUPS"
            "HIST_REDUCE_BLANKS"
        ];
    };
}
