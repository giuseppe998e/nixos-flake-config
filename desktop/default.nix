{ config, lib, home-manager, ... }:

let
    osStateVersion = config.system.stateVersion;

    xserver = config.services.xserver;
    gnomeEnabled = xserver.desktopManager.gnome.enable;
    gdmEnabled = xserver.displayManager.gdm.enable;
in {
    # System wide configurations
    imports = [
        ./fonts
        ./gnome
        ./pipewire
        ./starship
    ];

    # User specific configurations
    home-manager.users = {
        giuseppe = {
            imports = [
                ./alacritty
                ./firefox-devedition
            ] ++ lib.optionals gnomeEnabled [ ./gnome/theme.nix ];

            home.stateVersion = osStateVersion;
        };

        # Login screen theme
        gdm = lib.mkIf (gdmEnabled && gnomeEnabled) {
            imports = [ ./gnome/theme.nix ];
            home.stateVersion = osStateVersion;
        };

        root.home.stateVersion = osStateVersion;
    };
}
